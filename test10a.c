#include<stdio.h>
#include<stdlib.h>

int main()
{
	char fname[20];
	FILE* input;
	char ch;
	
	printf("ENTER THE NAME OF THE FILE \n");
	scanf("%s",fname);
	
	input=fopen(fname,"w");
	
	if(input==NULL)
	{
		printf("\n\n THE FILE IS NOT CREATED\n\n");
		exit(0);
	}
	
    printf("THE FILE IS CREATED SUCESSFULLY.\n\n");
    printf("ENTER THE  CHRACTERS IN THE FILE or PRESS <'0'>TO EXIT\n\n");
	while((ch=getchar())!='0')
	{	
		putc(ch,input);
	}
	printf("THE CONTENT IS WRITTEN SUCESSFULLY.\n\n");
	fclose(input);
	input=fopen(fname,"r");
	if(input==NULL)
	{
		printf("\n\n THE FILE CAN NOT FOUND\n\n");
		exit(0);
	}
	
	printf("THE CONTENTS OF THE FILE IS :\n\n");
	while((ch=getc(input))!= EOF)
	{
	printf("%c",ch);
}
	fclose(input);
	return 0;
	
}